#!/bin/bash

# Multiarch
DEB_HOST_MULTIARCH=$(dpkg-architecture -qDEB_HOST_MULTIARCH)

TEMP=$(getopt -l 'pyver:,srcdir:,builddir:,modules:' -- "$@")
if $?; then
        echo 'Terminating...' >&2
        exit 1
fi
echo "$TEMP"

# Note the quotes around "$TEMP": they are essential!
eval set -- "$TEMP"
unset TEMP

while true; do
    case "$1" in
        --pyver)    pyver="$2"                   ; shift 2 ;;
        --srcdir)   srcdir=$(readlink -f "$2")   ; shift 2 ;;
        --builddir) builddir=$(readlink -f "$2") ; shift 2 ;;
        --modules)  modules="$2"                 ; shift 2 ;;
        --) shift ; break ;;
        *)
            echo "ERROR: Illegal option --$OPTARG" >&2
            exit 1
            ;;
    esac
done


# build paths
pybuilddir=$(pybuild --system custom -p "$pyver" --print "{build_dir}")

# install path's
prefix=/usr
destdir=$(pybuild --system custom -p "$pyver" --print "{destdir}")
bindir=$prefix/bin
pkglibdir=$prefix/lib/cctbx/python"$pyver"
libdir=$prefix/lib/$DEB_HOST_MULTIARCH
pythondir=$(pybuild --system custom -p "$pyver" --print "{install_dir}")
incdir=$prefix/include

cd "$(dirname "${BASH_SOURCE[0]}")" || exit

echo "Intalling CCTBX into a FHS system"
echo "Modules: $modules"
echo ""
echo "From the source $srcdir"
echo "         build  $builddir"
echo ""
echo "To the FHS :"
echo "         pyver      $pyver"
echo "         bindir     $bindir"
echo "         incdir     $incdir"
echo "         pkglibdir  $pkglibdir"
echo "         libdir     $libdir"
echo "         pythondir  $pythondir"

#####################
# install pkglibdir #
#####################

install -d "$destdir$pkglibdir"

# fix libtbx_env

PYTHONPATH="$pybuilddir" LIBTBX_BUILD="$builddir" python"$pyver" update_libtbx_env.py --dest-dir="$destdir"

# install test files

cat <<EOF | rsync -avr --prune-empty-dirs --include-from="-" "$builddir"/ "$destdir$pkglibdir"
+ */
+ boost_adaptbx/tests/tst_optional_copy
+ fable/test/valid/file_names_and_expected_cout
+ iotbx/pdb/hybrid_36_fem
+ scitbx/array_family/tst_accessors
+ scitbx/array_family/tst_af_1
+ scitbx/array_family/tst_af_2
+ scitbx/array_family/tst_af_3
+ scitbx/array_family/tst_af_4
+ scitbx/array_family/tst_af_5
+ scitbx/array_family/tst_mat3
+ scitbx/array_family/tst_optional_copy
+ scitbx/array_family/tst_rectangular_full_packed
+ scitbx/array_family/tst_ref_matrix_facet
+ scitbx/array_family/tst_sym_mat3
+ scitbx/array_family/tst_unsigned_float_arithmetic
+ scitbx/array_family/tst_vec3
+ scitbx/error/tst_error
+ scitbx/lbfgs/tst_lbfgs
+ scitbx/math/tests/tst
+ scitbx/matrix/tests/tst_cholesky
+ scitbx/matrix/tests/tst_givens
+ scitbx/matrix/tests/tst_householder
+ scitbx/matrix/tests/tst_svd
+ scitbx/serialization/tst_base_256
+ smtbx/refinement/constraints/tests/tst_geometrical_hydrogens
+ smtbx/refinement/constraints/tests/tst_reparametrisation
+ smtbx/refinement/constraints/tests/tst_special_position
- *
EOF

##################
# install bindir #
##################

install -d "$destdir$bindir"

# install binaries

for d in "$builddir"/bin "$builddir"/exe_dev; do
    install "$d/"* "$destdir$bindir"
done

# fix all dispatchers once installed

sed -i -e '/export LIBTBX_BUILD/d' "$destdir$bindir"/*
sed -i -e 's,/../../../,/lib/python3/dist-packages/,g' "$destdir$bindir"/*

##################
# install libdir #
##################

install -d "$destdir$libdir"

# install the libraries with soversion numbers

cat <<EOF | rsync -avr --prune-empty-dirs --include-from="-" "$builddir"/lib/ "$destdir$libdir"
+ */
- *_ext*
- *dxtbx_flumpy*
+ *.so
+ *.so.*
- *
EOF

#####################
# install incdir #
#####################

install -d "$destdir$incdir"

# install all the headers from the src

for m in $modules; do
    case $m in
        annlib_adaptbx) from="$srcdir/$m/include/$m" ;;
        dxtbx) from="$srcdir/$m/src/$m";;
        fable) from="$srcdir/$m/";;
        *) from="$srcdir/$m" ;;
    esac
    cat <<EOF | rsync -avr --prune-empty-dirs --include-from="-" "$from" "$destdir$incdir"
+ */
+ *.h
+ *.hpp
- *
EOF
done

# install the generated headers during the build process

cat <<EOF | rsync -avr --prune-empty-dirs --include-from="-" "$builddir"/include/ "$destdir$incdir"
+ */
+ *.h
+ *.hpp
- *
EOF

#####################
# install pythondir #
#####################

install -d "$destdir$pythondir"

# install all the command_line non python files

for m in $modules; do
    case $m in
        dxtbx) from="$srcdir/$m/src/$m" ;;
        *) from="$srcdir/$m";;
    esac
    cat <<EOF | rsync -avr --prune-empty-dirs --include-from="-" "$from" "$destdir$pythondir"
+ */
+ **/command_line/*
- *
EOF
done

# install data test files under pythondir

for m in $modules; do
    cat <<EOF | rsync -avr --prune-empty-dirs --include-from="-" "$srcdir/$m" "$destdir$pythondir"
; default files
+ */
+ **/tst*
- *
EOF
done

# install specific data files for tests under pythondir

cat <<EOF | rsync -avr --prune-empty-dirs --include-from="-" "$srcdir"/ "$destdir$pythondir"
+ */
- debian/***
+ annlib_adaptbx/tests/***
+ cma_es/cma/initials.par
+ fable/fem/major_types.hpp
+ fable/test/***
+ iotbx/bioinformatics/test/alignment.pir
+ iotbx/bioinformatics/test/ebi_blast.xml.gz
+ iotbx/bioinformatics/test/ncbi_blast.xml.gz
+ iotbx/ccp4_map/ccp4_20_21_22_to_30_40_50.ccp4
+ iotbx/examples/pdb_truncate_to_ala/***
+ iotbx/regression/data/***
+ iotbx/xds/tests/INTEGRATE.HKL
+ iotbx/xds/tests/NEW_XPARM.XDS
+ iotbx/xds/tests/XDS.INP
+ iotbx/xds/tests/XDS_2.INP
+ iotbx/xds/tests/XPARM.XDS
+ libtbx/citations.params
+ mmtbx/regression/pdbs/***
+ rstbx/indexing/results2.dat
+ rstbx/indexing/si_brief.dat
+ rstbx/indexing/si_synthetic.dat
+ scitbx/array_family/boost_python/regression_test.py
+ scitbx/tests/***
+ smtbx/regression/test_data/sucrose_p1.res
+ smtbx/regression/test_data/thpp.cif
+ smtbx/regression/test_data/thpp.ins
+ smtbx/regression/test_data/thpp.hkl
- *
EOF
